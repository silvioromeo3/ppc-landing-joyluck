import React, { useState } from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import { HiOutlineMenuAlt4 } from 'react-icons/hi';
import Logo from '../assets/logo.png';

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const [logo, setLogo] = useState(false);
  const handleNav = () => {
    setNav(!nav);
    setLogo(!logo);
  };
  const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className="flex justify-between items-center h-12 mx-auto px-4 bg-slate-500">
      <img src={Logo} alt="JLC" className="h-40 w-60 mt-20" />
      <ul className="hidden md:flex cursor-pointer text-lg">
        <li>Home</li>
        <li className="nav-item hover:text-gray-700 cursor-pointer" onClick={handleClickScroll('about')}>
          About
        </li>
        <li className="nav-item hover:text-gray-700 cursor-pointer" onClick={handleClickScroll('contact')}>
          Contact Us
        </li>
      </ul>

      {/* Hamburger */}
      <div onClick={handleNav} className="md:hidden z-10">
        {nav ? <AiOutlineClose className="text-black" size={20} /> : <HiOutlineMenuAlt4 size={20} />}
      </div>

      {/* Mobile menu dropdown */}
      <div
        onClick={handleNav}
        className={nav ? 'absolute text-black left-0 top-0 w-full bg-gray-100/90 px-4 py-7 flex flex-col' : 'absolute left-[-100%]'}
      >
        <ul>
          <h1>JLC</h1>
          <li className="border-b">Home</li>
          <li onClick={handleClickScroll('about')} className="border-b">
            About
          </li>
          <li onClick={handleClickScroll('pricing')} className="border-b">
            Pricing
          </li>
          <li onClick={handleClickScroll('contact')} className="border-b">
            Contact Us
          </li>
          <div className="flex flex-col"></div>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
